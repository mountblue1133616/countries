import { useState, useEffect } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import DetailedCard from "./components/DetailedCard";
import HomePage from "./components/HomePage";
import axios from "axios";
import "./App.css";

function App() {
  const [countries, setCountries] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");
  const [selectedRegion, setSelectedRegion] = useState("");
  const [darkMode, setDarkMode] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const mode = darkMode ? "dark-mode" : "light-mode";
  const toggleMode = () => {
    setDarkMode(!darkMode);
  };

  const getUniqueRegions = (countries) => {
    const uniqueRegions = [];
    countries.forEach((country) => {
      if (!uniqueRegions.includes(country.region)) {
        uniqueRegions.push(country.region);
      }
    });
    return uniqueRegions;
  };

  useEffect(() => {
    setIsLoading(true);
    axios
      .get("https://restcountries.com/v3.1/all")
      .then((response) => {
        setCountries(response.data);
        setIsLoading(false);
      })
      .catch((error) => {
        console.log(error);
        setIsLoading(false);
        setIsError(true);
      });
  }, []);

  const handleSearchChange = (event) => {
    setSearchTerm(event.target.value);
  };

  const handleRegionChange = (event) => {
    setSelectedRegion(event.target.value);
  };

  const allRegions = getUniqueRegions(countries);

  const filteredCountries = countries.filter(
    (country) =>
      country.name.common.toLowerCase().startsWith(searchTerm.toLowerCase()) &&
      (selectedRegion === "" || country.region === selectedRegion)
  );

  return (
    <Router>
      <div className={mode}>
        <div className="country-grid ">
          <div className="header">
            <h4>Where in the world?</h4>
            <button onClick={toggleMode}>
              {darkMode ? "Light Mode" : "Dark Mode"}
            </button>
          </div>
          {isLoading ? (
            <div>Loading...</div>
          ) : isError ? (
            <div>Error occurred while fetching data.</div>
          ) : (
            <Routes>
              <Route
                path="/"
                element={
                  <HomePage
                    countries={countries}
                    searchTerm={searchTerm}
                    handleSearchChange={handleSearchChange}
                    selectedRegion={selectedRegion}
                    handleRegionChange={handleRegionChange}
                    allRegions={allRegions}
                    filteredCountries={filteredCountries}
                    isLoading={isLoading}
                  />
                }
              />
              <Route path="country/:code" element={<DetailedCard />} />
            </Routes>
          )}
        </div>
      </div>
    </Router>
  );
}

export default App;
