/* eslint-disable react/prop-types */
import SearchFilter from "./SearchFilter";
import Country from "./Country";

const HomePage = ({
  searchTerm,
  handleSearchChange,
  selectedRegion,
  handleRegionChange,
  allRegions,
  getUniqueRegions,
  filteredCountries,
}) => {
  return (
    <>
      <SearchFilter
        searchTerm={searchTerm}
        handleSearchChange={handleSearchChange}
        selectedRegion={selectedRegion}
        handleRegionChange={handleRegionChange}
        allRegions={allRegions}
        getUniqueRegions={getUniqueRegions}
        filteredCountries={filteredCountries}
      />
      <div className="countries-container">
        {filteredCountries.length !== 0 ? (
          filteredCountries.map((country) => (
            <Country
              key={country.ccn3}
              code={country.cca3}
              name={country.name.common}
              img_path={country.flags.png}
              population={country.population}
              region={country.region}
              capital={country.capital && country.capital[0]}
            />
          ))
        ) : (
          <h1>No items found</h1>
        )}
      </div>
    </>
  );
};

export default HomePage;
