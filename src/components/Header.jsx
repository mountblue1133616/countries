// eslint-disable-next-line react/prop-types
const Header = ({ toggleMode, darkMode }) => {
  return (
    <div className="header">
      <h4>Where in the world?</h4>
      <button onClick={toggleMode}>
        {darkMode ? "Light Mode" : "Dark Mode"}
      </button>
    </div>
  );
};

export default Header;
