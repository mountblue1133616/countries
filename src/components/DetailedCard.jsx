import { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";
import axios from "axios";

const DetailedCard = () => {
  const { code } = useParams();
  const [country, setCountry] = useState(null);

  useEffect(() => {
    axios
      .get(`https://restcountries.com/v3.1/alpha/${code}`)
      .then((response) => {
        console.log(response.data[0].name.common);
        setCountry(response.data[0]);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [code]);

  if (!country) {
    return null; // Handle loading state or error
  }

  return (
    <>
      <div className="detailed-container">
        <Link to="/">
          <button>Back</button>
        </Link>
        <div className="select-country-container">
          <div className="single-flag-img">
            <img src={country.flags.png} alt="" />
          </div>
          <div className="single-country-details">
            <h4 className="single-country-name">{country.name.common}</h4>
            <ul className="single-country-details-list">
              <div className="list-1">
                <li>Official Name: {country.name.official}</li>
                <li>Native Name: {country.name.nativeName?.ara?.common}</li>
                <li>Population: {country.population}</li>
                <li>Region: {country.region}</li>
                <li>Sub Region: {country.subregion}</li>
                <li>Capital: {country.capital && country.capital[0]}</li>
              </div>
              <div className="list-2">
                <li>
                  Currencies:
                  {Object.values(country.currencies)
                    .map((currency) => currency.name)
                    .join(", ")}
                </li>
                <li>Top Level Domain: {country.tld?.[0]}</li>
                <li>
                  Languages :{Object.values(country.languages).join(", ")}
                </li>
              </div>
            </ul>
            <div className="single-country-borders">
              {country.borders && country.borders.length > 0 ? (
                country.borders.map((border) => (
                  <Link to={`/country/${border}`} key={border}>
                    <button className="border-btn">{border}</button>
                  </Link>
                ))
              ) : (
                <p>No borders found.</p>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default DetailedCard;
