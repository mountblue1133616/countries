/* eslint-disable react/prop-types */

const SearchFilter = ({
  searchTerm,
  handleSearchChange,
  selectedRegion,
  handleRegionChange,
  allRegions,
}) => {
  return (
    <div className="search-filter">
      <input
        type="text"
        placeholder="Search for country"
        value={searchTerm}
        onChange={handleSearchChange}
      />
      <div className="filter">
        <label htmlFor="region">Select Region: </label>
        <select value={selectedRegion} onChange={handleRegionChange}>
          <option value="">All</option>
          {allRegions &&
            allRegions.map((region) => (
              <option key={region} value={region}>
                {region}
              </option>
            ))}
        </select>
      </div>
    </div>
  );
};

export default SearchFilter;
