/* eslint-disable react/prop-types */
import { Link } from "react-router-dom";

const Country = ({ code, name, img_path, population, region, capital }) => {
  return (
    <Link to={`/country/${code}`}>
      <div className="single-country">
        <div className="flag">
          <img src={img_path} alt="flag" className="flag-img" />
        </div>
        <h2 id="name-country">{name}</h2>
        <div className="details">
          <span>Population: {population}</span>
          <span>Region: {region}</span>
          <span>Capital: {capital}</span>
        </div>
      </div>
    </Link>
  );
};

export default Country;
